# PC Backup and Restore tool

## About
This tool allows you to backup and restore data and settings.

## Limitations
  - Windows only solution
    - Windows 7
    - Windows 8/8.1
    - Windows 10
  - **You have to run app with elevated privileges**

## Contact

|             |                       |     |
| ----------- | --------------------- | --- |
| Petr Radouš | <darroue@gmail.com>   |
|             | https://darroue.cloud |
|             |                       |

## Usage
Just run tool as admin and follow what app asks you to do :)
