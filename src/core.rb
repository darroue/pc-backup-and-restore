$header = 'This tool allows you to backup and restore files and settings.

It can be used for instance:
- When you want to transfer user data to new computer
- When you need to recreate user profile

// Created by Petr Radouš <darroue@gmail.com> at 2020
'

if `reg query HKU\\S-1-5-19 2>&1`.encode('utf-8', invalid: :replace, undef: :replace, replace: '?') =~ /ERROR/
  puts 'ERROR: You have to run application with elevated rights.'
  puts 'Right mouse button -> Run as Administrator'
  sleep 5
  abort
end

Dir.chdir(File.dirname(__FILE__))

%w[menu executors module].each do |folder|
  Dir.glob(File.join(folder, '*.rb')).each do |file|
    require_relative file
  end
end

include FilePaths
include FileManager
include InputArguments

proccess_arguments

begin
  MainMenu.new.show(lambda { |selected|
    selected.each do |option|
      if option == :backup
        BackupMenu.new.show(lambda { |selected|
          Backup.new.proccess_commands(selected)
        }, selected: :defaults)
        system "explorer #{external_filepath(file_path)}"
      elsif option == :restore
        menu = RestoreMenu.new(file_path)
        menu.show(lambda { |selected|
          Restore.new(menu.restore_path).proccess_commands(selected)
        }, selected: :defaults, message: 'Options not listed here are not available!')
      end
    end
  })

  puts 'All done :)'
rescue Interrupt => e
  puts 'Control + C pressed, quiting.. Bye ^_^'
end
