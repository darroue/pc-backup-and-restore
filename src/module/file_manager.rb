require 'fileutils'

module FileManager
  def external_filepath(path)
    path.gsub('/', '\\')
  end

  def internal_path(path)
    path.gsub('\\', '/')
  end

  def bytesToKiloBytes(number)
    number / 1000
  end

  def bytesToMegaBytes(number)
    bytesToKiloBytes(number) / 1000
  end

  def bytesToGigaBytes(number)
    bytesToMegaBytes(number) / 1000
  end

  def human_size(number)
    number = number.to_f

    number, unit = if number >= (1000 * 1000 * 1000)
                     [bytesToGigaBytes(number), 'GB']
                   elsif number >= (1000 * 1000)
                     [bytesToMegaBytes(number), 'MB']
                   elsif number >= (1000)
                     [bytesToKiloBytes(number), 'KB']
                   else
                     [number, 'Bytes']
                   end

    number.round(2).to_s + unit
  end

  def get_intend(intend)
    ' ' * intend
  end

  def copy_entry(source_path, target_dir = nil, quiet: false, intend: 2)
    basename = File.basename(source_path)
    backup_dir = target_dir || $backup_dir
    target_path = File.join(backup_dir, basename)
    nested_intend = intend * 2

    puts(get_intend(intend) + "> #{basename}")

    if File.directory?(source_path)
      paths = Dir.glob(File.join(source_path, '**/*'))
      sizes = []
      file_count = 0
      files = paths.each_with_object([]) do |path, array|
        next unless File.file?(path)

        next unless file_size = File.size?(path)

        sizes << file_size
        array << path
        file_count += 1
      end

      if files.any?
        unless quiet
          puts(get_intend(nested_intend) + "Found #{file_count} file(s) with total size #{human_size(sizes.sum)}")
        end
        print(get_intend(nested_intend) + "0% done\r") unless quiet

        i_need(100, files).each.with_index do |slice, index|
          if slice.any?
            slice.each do |path|
              target_file_path = path.gsub(source_path, target_path)

              begin
                FileUtils.mkdir_p(File.dirname(target_file_path))
                FileUtils.copy_entry(path, target_file_path)
              rescue StandardError => e
                @file_errors << [path, e.class]
              end
            end
          end

          print(get_intend(nested_intend) + "#{index + 1}% done\r") unless quiet
        end

        puts "\n" unless quiet
      else
        puts(get_intend(nested_intend) + '# No files were found!') unless quiet
      end
    else
      FileUtils.copy_entry(source_path, target_path)
    end
  end

  def copy_entries(source_paths, target_dir = nil, quiet: false, intend: 2)
    @file_errors ||= []

    source_paths.each do |path|
      copy_entry(path, target_dir, quiet: quiet, intend: intend)
    end

    if @file_errors.any?
      :"Unable to copy files:\n#{@file_errors.to_table}"
    else
      ''
    end
  end

  def i_need(bits, array)
    c = array.count
    (1..bits - 1).map { |i| array.shift((c + i) * 1.0 / bits) } + [array]
  end
end
