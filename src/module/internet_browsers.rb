module InternetBrowsers
  def backup_internet_browsers(backup_path)
    source_paths = internet_browser_paths[:backup]
    existing_paths = {}
    source_paths.each_pair do |browser, paths|
      existing_paths[browser] = []

      Dir.glob(paths).each do |path|
        existing_paths[browser] << path if File.exist?(path)
      end
    end

    if existing_paths.values.flatten.any?
      existing_paths.each do |browser, paths|
        next if paths.empty?

        puts "  Backing up files for #{browser}"
        copy_entries(paths, File.join(backup_path, browser.to_s), intend: 4)
      end
    else
      :"No files to backup were found"
    end
  end

  def restore_internet_browsers(backup_path)
    source_paths = Dir.glob(File.join(backup_path, '*'))
    target_paths = internet_browser_paths[:restore]

    if source_paths.any?
      source_paths.each do |browser_path|
        paths = Dir.glob(File.join(browser_path, '*'))
        next if paths.empty?

        browser = File.basename(browser_path)
        puts "  Restoring files for #{browser}"
        copy_entries(paths, target_paths[browser.downcase.to_sym], intend: 4)
      end
    else
      :"No files to restore were found"
    end
  end

  def internet_browser_paths
    {
      backup: {
        google_chrome: internal_path(File.join(ENV['localappdata'], 'Google/Chrome/User Data/{Default,Profile*}')),
        mozilla_firefox: internal_path(File.join(ENV['appdata'], 'Mozilla/Firefox/{Profiles,profiles.ini}')),
        internet_explorer: internal_path(File.join(ENV['userprofile'], 'Favorites'))
      },
      restore: {
        google_chrome: internal_path(File.join(ENV['localappdata'], 'Google/Chrome/User Data/')),
        mozilla_firefox: internal_path(File.join(ENV['appdata'], 'Mozilla/Firefox/')),
        internet_explorer: internal_path(File.join(ENV['userprofile']))
      }
    }
  end
end
