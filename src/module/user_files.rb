module UserFiles
  def default_user_files
    %w[
      Contacts
      Desktop
      Documents
      Downloads
      Favorites
      Links
      Music
      Pictures
      Videos
    ]
  end

  def get_entities_and_selected(path, defaults)
    selected = []
    entities = Dir.glob(
      internal_path(path) + '/*',
      File::FNM_DOTMATCH
    )[2..-1]
                  .each_with_object({}).with_index do |(key, hash), index|
      unless key.downcase.include?('ntuser')
        hash[key] = key
        selected << index.to_s if defaults.include?(File.basename(key))
      end
    end

    [entities, selected]
  end

  def user_files_menu(source_path, target_path, menu)
    entities, selected = get_entities_and_selected(source_path, default_user_files)

    return :"No files to backup were found!" if entities.empty?

    menu.new(entities, selected).show(lambda { |selected|
      system('cls')
      puts '> Copying User files'

      copy_entries(selected, target_path)
    }, selected: selected)
  end
end
