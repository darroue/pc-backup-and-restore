module FilePaths
  def root_dir
    Dir.pwd
  end

  def file_path
    path = File.join(root_dir, '..', 'BACKUPS', ENV['computername'])
    FileUtils.mkdir_p(path)

    path
  end

  def dependency_path
    File.join(root_dir, 'vendor/dep/')
  end
end
