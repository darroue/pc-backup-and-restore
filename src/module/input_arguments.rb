require_relative '../menu/backup_and_restore'
require_relative 'user_files'

module InputArguments
  include UserFiles

  def input_arguments
    @input_arguments ||= ARGV.map do |argument|
      argument = argument.downcase

      if argument.start_with?('-fl=')
        path = internal_path(argument.delete("'").delete('"').sub('-fl=', ''))
        @file_list_path = path if File.exist?(path)

        '-fl'
      else
        argument
      end
    end
  end

  def valid_arguments
    input_arguments & allowed_arguments
  end

  def proccess_arguments
    if input_arguments.any?
      if valid_arguments.any?
        valid_arguments.each do |argument|
          case argument
          when '-h'
            show_help
            abort
          when '-b'
            @mode = :backup
          when '-r'
            @mode = :restore
          when '-o'
            @options = valid_options
          end
        end

        if @mode
          if @mode == :backup
            selected_options = (@options || available_option_keys).map(&:to_sym)

            if selected_options.include?(:user_files)
              selected_options.delete(:user_files)

              file_paths, indexes = get_entities_and_selected(
                ENV['userprofile'],
                @file_list_path ? File.read(@file_list_path).split("\n").map(&:chomp) : default_user_files
              )

              file_paths = file_paths.map(&:first)
              indexes = indexes.map(&:to_i)

              file_paths = file_paths.to_a.each_with_object([]).with_index do |(path, object), index|
                object << path if indexes.include?(index)
              end

              copy_entries(
                file_paths,
                File.join(file_path, 'user_files')
              )
            end

            Backup.new.proccess_commands(selected_options)
          end
        else
          show_help
          puts "# Error: Mode weren't selected!"
        end
      else
        show_help
      end
      abort
    end
  end

  def valid_options
    valid_arguments & available_option_keys
  end

  def available_options
    @available_options ||= BackupAndRestoreMenu.new.options
  end

  def available_option_keys
    @available_option_keys ||= available_options.keys.map(&:to_s)
  end

  def allowed_arguments
    %w[-h -b -r -o -fl] + available_option_keys
  end

  def show_help
    rows = ([
      ['-h', { value: 'Show this help', colspan: 2 }],
      :separator,
      ['-b', { value: 'Backup mode', colspan: 2 }],
      # ['-r', {:value => 'Restore mode', :colspan => 2}],
      ['-fl=""', { value: 'Path to file containing list of files to backup', colspan: 2 }],
      ['-o', { value: 'Options you want to proccess, separated by space', colspan: 2 }],
      [nil, { value: '(if not specified, then all options are set)', colspan: 2 }],
      :separator
    ] + available_options.map(& ->(k, v) { ['', k, v] }))

    puts [
      $header,
      "\nYou can run this app in non-interactive mode by using following arguments:",
      Text::Table.new(
        rows: rows,
        vertical_boundary: '',
        horizontal_boundary: '',
        boundary_intersection: ''
      )
    ]
  end
end
