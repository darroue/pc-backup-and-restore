require 'text-table'

class Menu
  attr_accessor :title, :options, :default_options, :allowed_inputs, :number_inputs, :only_one

  CONTROLS = {
    number: 'Select option(s) (You can specify multiple delimited by space)',
    d: 'Select only Defaults',
    a: 'Select All of the above',
    n: 'Select None of the above',
    c: '> Continue',
    q: '# Quit'
  }.freeze

  def initialize
    @controls_without_number = CONTROLS.select(& ->(k, _v) { k != :number })
    @number_inputs = [*0..(@options.count - 1)]
    @number_inputs_s = @number_inputs.map(&:to_s)
    @allowed_inputs = @number_inputs_s + @controls_without_number.keys.map(&:to_s)
  end

  def validate_input(value, allowed_inputs)
    value.select { |e| allowed_inputs.include?(e) }
  end

  def get_option_values(values)
    @options.keys.select.with_index { |_, index| values.include?(index) }
  end

  def option_processing(method_call, selected)
    values = validate_input(selected, @number_inputs_s).uniq.sort.map(&:to_i)
    values = [values.first] if only_one

    if values.any?
      method_call.call(get_option_values(values))
    else
      show(method_call, selected: values, message: 'No options selected!')
    end
  end

  def show(method_call, selected: [], message: nil)
    selected = default_options if selected == :defaults

    @controls_without_number.each_key do |key|
      next unless selected.include?(key.to_s)

      if key == :c
        option_processing(method_call, selected)
        return
      end

      if key == :q
        puts 'Exiting... Bye! :)'
        abort
      end

      next if only_one

      selected = if key == :a
                   @number_inputs_s
                 elsif key == :n
                   []
                 elsif key == :d
                   @number_inputs_s & default_options
                 end
      break
    end

    if only_one && selected.any?
      option_processing(method_call, selected)
      return
    end

    system('cls')

    select_message = only_one ? 'Select one of the options:' : 'Select one or more options:'

    table = (@options.map.with_index do |(_option, value), index|
      [item_mark(selected.include?(index.to_s)), "#{index}) #{value}".ljust(69)]
    end).to_table

    controls = (only_one ? CONTROLS.slice(:number, :q) : CONTROLS).map do |key, value|
      if key == :number
        value = 'Select option' if only_one
        key = "0-#{@options.count - 1}"
      end

      "#{key})".rjust(6).ljust(8) + value
    end

    message = "\n# #{message}" if message

    puts [
      title,
      nil,
      select_message,
      table,
      'Controls:',
      controls,
      message
    ].join("\n")
    print "\n> "

    user_input = validate_input(gets.encode(
      'utf-8',
      invalid: :replace,
      undef: :replace,
      replace: '?'
    ).split("\s"), @allowed_inputs)

    selection = (selected - user_input) + user_input.reject(& ->(i) { selected.include?(i) })
    show(method_call, selected: selection)
  end

  def item_mark(selected = false)
    selected ? '[x]' : '[ ]'
  end
end
