class Executor
  attr_accessor :data

  def execute_command(name, command, path, command_type = :external, intend: 0, no_label: false)
    intend = ' ' * intend

    puts intend + "> #{'Executing ' unless no_label}#{name}"

    FileUtils.mkdir_p(File.dirname(path)) if path

    command_response = if command_type.to_s.include?('internal')
                         command.call
                       else
                         `#{command}`
                       end

    if !error?(command_response)
      File.open(path, 'w') { |f| f.write(command_response) } unless command_type.to_s.include?('no_log')
    else
      error_message = command_response.to_s

      @errors << name if @errors

      print intend + '# error > '
      puts error_message
    end
  end

  def proccess_commands(commands)
    @errors = []

    commands.each do |command_name|
      execute_command(*data[command_name])
      puts "\n"
    end

    if @errors.any?
      puts "\nFinished with errors:\n" +
           @errors.map(& ->(command_name) { ' - ' + command_name }).join("\n")
    end
  end

  def error?(message)
    return true if message.is_a?(Symbol)

    keywords = [
      'error',
      'failed',
      'failure',
      'not running'
    ]

    keywords.each do |keyword|
      return true if message.to_s.encode('utf-8', invalid: :replace, undef: :replace,
                                                  replace: '?').downcase.include?(keyword)
    end

    false
  end
end
