require_relative '../lib/menu'

class MainMenu < Menu
  def initialize
    self.options = {
      backup: 'Backup',
      restore: 'Restore'
    }

    self.title = $header
    self.only_one = true

    super
  end
end
