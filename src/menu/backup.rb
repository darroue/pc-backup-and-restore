require_relative 'backup_and_restore'

class BackupMenu < BackupAndRestoreMenu
  def initialize
    super

    self.default_options = number_inputs.map(&:to_s).first(6)
  end
end
