require_relative 'backup_and_restore'

class RestoreMenu < BackupAndRestoreMenu
  attr_reader :restore_path
  attr_accessor :file_path

  def initialize(file_path)
    @file_path = file_path

    super()

    get_available_options
  end

  def restore_path
    @restore_path ||= begin
      unless File.exist?(@file_path)
        restore_path = File.join(@file_path, '..', 'restore')

        if File.exist?(restore_path)
          @file_path = restore_path
        else
          put_and_abort(
            "# Restore folder were not found! None of the expected paths exists \n- #{@file_path}\n- #{restore_path}" +
            "\nIf computer name changed since backup or this is other PC, then you have to rename backup folder to 'restore'"
          )
        end
      end

      @file_path
    end
  end

  def get_available_options
    available_options = (options.keys.map(&:to_s) & Dir.glob(File.join(restore_path, '*')).map do |file|
      option = File.basename(file, File.extname(file)).downcase

      if %w[
        app_list
        product_keys
      ].include?(option)
        next
      end

      option
    end)

    if available_options.any?
      self.options = options.slice(*available_options.map(&:to_sym))
    else
      put_and_abort("# Backup folder found, but it doesn't contain anything that i can restore :(")
    end
  end

  def put_and_abort(message)
    puts message

    abort
  end
end
