require_relative '../lib/menu'

class BackupAndRestoreMenu < Menu
  def initialize
    self.title = "[#{self.class.name.sub('Menu', '')}]"
    self.options = {
      user_files: 'User files',
      internet_browsers: 'Internet Browsers',
      network_drives: 'Network drives',
      certificates: 'Certificates',
      wifi_profiles: 'WiFi profiles',
      printers: 'Printers Drivers and Queues',
      drivers: 'Windows Drivers',
      app_list: 'List of installed applications',
      product_keys: 'Windows and Office Product Keys'
    }
    self.default_options = []

    super
  end
end
