require_relative '../lib/executor'

%w[file_paths user_files internet_browsers].each do |module_name|
  require_relative '../module/' + module_name
end

class BackupAndRestoreExecutor < Executor
  include UserFiles
  include InternetBrowsers
  include FilePaths
end
