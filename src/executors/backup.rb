require_relative '../lib/menu'
require_relative 'backup_and_restore'

class FileBackupMenu < Menu
  def initialize(options, default_options)
    self.title = 'What files do you want to back up?'
    self.options = options
    self.default_options = default_options

    super()
  end
end

class Backup < BackupAndRestoreExecutor
  def initialize
    self.data = {
      user_files: [
        'User files',
        lambda {
          user_files_menu(ENV['userprofile'], File.join(file_path, 'user_files'), FileBackupMenu)
        },
        nil,
        :internal_no_log
      ],
      internet_browsers: [
        'Internet Browsers',
        lambda {
          backup_internet_browsers(File.join(file_path, 'internet_browsers'))
        },
        nil,
        :internal_no_log
      ],
      network_drives: [
        'Network drives',
        lambda {
          mapped_drives = `reg query "HKCU\\Network"`.split("\n").reject { |i| i.empty? }
          network_drives = mapped_drives.each_with_object({}) do |drive, object|
            drive_letter = File.basename(drive).to_sym
            drive_path = `reg query "#{drive}" /v RemotePath`.split(/\s+/).reject { |i| i.empty? }.last

            object[drive_letter] = drive_path
          end

          if network_drives.any?
            network_drives.map { |drive, path| "net use #{drive.downcase}: #{path}" }.join("\n")
          else
            :"No network drivers found!"
          end
        },
        File.join(file_path, 'network_drives.cmd'),
        :internal
      ],
      certificates: [
        'Certificates',
        external_filepath(dependency_path + 'CertMig.exe') +
          " -all -e \"#{external_filepath(File.join(file_path, 'certificates'))}\"",
        File.join(file_path, 'certificates', '.'),
        :external_no_log
      ],
      wifi_profiles: [
        'WiFi profiles',
        "netsh wlan export profile key=clear folder=\"#{external_filepath(File.join(file_path, 'wifi_profiles'))}\"",
        File.join(file_path, 'wifi_profiles', '.'),
        :external_no_log
      ],
      printers: [
        'Printers Drivers and Queues',
        (
          filename = 'Printers.PrinterExport'
          "%WINDIR%\\System32\\Spool\\Tools\\printbrm -B -F %tmp%\\#{filename} && move \"%tmp%\\#{filename}\" \"#{external_filepath(file_path)}\\\""
        ),
        nil,
        :external_no_log
      ],
      drivers: [
        'Windows Drivers',
        (
          win_ver = `ver`.match(/[\d.]+/).to_s

          if win_ver.start_with?('10') || win_ver.start_with?('6.1')
            "powershell -Command Export-WindowsDriver -Online -Destination '#{external_filepath(File.join(file_path,
                                                                                                          'drivers'))}'"
          else
            "rd /s /q \"#{external_filepath(File.join(file_path, 'drivers'))}\" && \\" +
            [
              [ENV['SystemRoot'], 'System32', 'DriverStore'],
              [ENV['SystemRoot'], 'System32', 'Drivers'],
              [ENV['SystemRoot'], 'System32', 'DrvStore'],
              [ENV['SystemRoot'], 'inf']
            ].map do |path_arr|
              %(\
                md \"#{external_filepath(File.join(file_path, 'drivers', path_arr.last))}\" \
                && copy \
                \"#{external_filepath(File.join(*path_arr))}\" \
                \"#{external_filepath(File.join(file_path, 'drivers', path_arr.last, '/'))}\" \
              )
            end.join(' && ')
          end

        ),
        File.join(file_path, 'drivers', '.'),
        :external_no_log
      ],
      app_list: [
        'List of installed applications',
        'wmic product get name,version',
        File.join(file_path, 'app_list.txt'),
        :external
      ],
      product_keys: [
        'Windows and Office Product Keys',
        external_filepath(dependency_path + 'produkey.exe') +
          " /OfficeKeys 1 /WindowsKeys 1 /stab \"#{external_filepath(File.join(file_path, 'product_keys.txt'))}\"",
        nil,
        :external_no_log
      ]
    }
  end
end
