require_relative '../lib/menu'
require_relative 'backup_and_restore'

class FileRestoreMenu < Menu
  def initialize(options, default_options)
    self.title = 'What files do you want to restore?'
    self.options = options
    self.default_options = default_options

    super()
  end
end

class Restore < BackupAndRestoreExecutor
  def initialize(file_path)
    self.data = {
      user_files: [
        'User files',
        lambda {
          user_files_menu(File.join(file_path, 'user_files'), ENV['userprofile'], FileRestoreMenu)
        },
        nil,
        :internal_no_log
      ],
      internet_browsers: [
        'Internet Browsers',
        lambda {
          restore_internet_browsers(File.join(file_path, 'internet_browsers'))
        },
        nil,
        :internal_no_log
      ],
      network_drives: [
        'Network drives',
        lambda {
          system external_filepath(File.join(file_path, 'network_drives.cmd')).to_s
        },
        nil,
        :internal_no_log
      ],
      certificates: [
        'Certificates',
        external_filepath(dependency_path + 'CertMig.exe') +
          " -all -i \"#{external_filepath(File.join(file_path, 'certificates'))}\"",
        nil,
        :external_no_log
      ],
      wifi_profiles: [
        'WiFi profiles',
        (
          command = Dir.glob(File.join(file_path, 'wifi_profiles', '*.xml')).map do |file|
            "netsh wlan add profile folder=\"#{external_filepath(file)}\""
          end.join(' && ')
          command.empty? ? 'echo Error: No wifi profiles to import' : command
        ),
        nil,
        :external_no_log
      ],
      printers: [
        'Printers Drivers and Queues',
        (
          filename = 'Printers.PrinterExport'
          "copy /-Y \"#{external_filepath(file_path)}\\#{filename}\" \"%tmp%\\\" && %WINDIR%\\System32\\Spool\\Tools\\printbrm -R -F %tmp%\\#{filename}"
        ),
        nil,
        :external_no_log
      ],
      drivers: [
        'Windows Drivers',
        (
          executable = internal_path(File.join(ENV['WINDIR'], 'System32', 'pnputil.exe'))

          executable = File.join(dependency_path, 'pnputil.exe') unless File.exist?(executable)

          "cd \"#{external_filepath(File.join(file_path,
                                              'drivers'))}\" && #{external_filepath(executable)} /add-driver *.inf /subdirs"
        ),
        nil,
        :external_no_log
      ]
    }
  end
end
